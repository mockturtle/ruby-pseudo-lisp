# ruby pseudo-LISP

This package defines a kind of LISP-like interpreter, useful to create fairly complex configuration files, something more than just a "key, value"  database, but nothing that requires a fully fledged language
