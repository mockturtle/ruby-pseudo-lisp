module Macro_File
  def Macro_File::load(filename)
    result = Hash.new
    
    lines = Macro_File::normalize(filename)

    while lines.length > 0
      macro_name = Macro_File::expect_begin(lines.shift)

      raise "Duplicated macro #{macro_name}" if result.has_key?(macro_name)

      macro_body = Array.new
      while lines.length > 0 && ! Macro_File::is_end_line(lines[0], macro_name)
        macro_body << lines
      end

      raise "Missing 'end #{macro_name}'"  if lines.empty?

      assert_eq(Macro_File::is_end_line(lines[0], macro_name), true)
      
      result[macro_name] = Macro_File::run_body(macro_body, result)
    end

    return result
  end

  def Macro_File::expect_begin(line)
    parts = line.split

    unless parts.length == 2 && parts[0] == 'begin' 
      raise "Expected begin line, found '#{line}'"
    end

    return parts[1]
  end

  def Macro_File::is_end_line(line, macro_name)
    parts = line.split

    return false unless parts.length == 2
    return false unless parts[0] == 'end'
    
    return true if part[1] == macro_name

    raise "Bad end line, expected '#{macro_name}', found #{parts[1]}"
  end

  def Macro_File::normalize(filename)
    result = Array.new

    buffer = ''
    lines = File.readlines(filename)
    lines.each do |line|
      line.strip!

      if line[0] == '#'
        #
        # A comment line close any currently open buffer
        #
        result << buffer unless buffer == ''
        buffer = ''
        next
      end

      buffer += line

      if buffer[-1] == '\\'
        buffer[-1] = ' '
      else
        result << buffer
        buffer = ''
      end
    end
  end
end
