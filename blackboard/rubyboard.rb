#!/usr/bin/env ruby

#
# Some Tk versions produce lots of annoying warnings about some
# rb_* functions that will be obsoleted later.  I think this is the
# only way to silence it.
#
$VERBOSE = nil

require "../pseudo_lisp.rb"
require 'tk'


def assert_eq(x, y, msg="Assert failed")
  if x != y then
    raise "#{msg} #{x} != #{y} in #{caller_locations}"
  end
end


def assert_leq(x, y, msg="Assert failed")
  if x > y then
    raise "#{msg} #{x} > #{y} in #{caller_locations}"
  end
end

def assert_geq(x, y, msg="Assert failed")
  if x < y then
    raise "#{msg} #{x} < #{y} in #{caller_locations}"
  end
end

class BBox
  def initialize(*args)
    raise "Bad call to BBox.new #{args}" if args.length > 1
    
    if args.length == 0
      @empty = true
      @top = @bottom = @left = @right = 0
      return 
    end

    data = args[0]

    unless data.is_a? Hash
      raise "Expecting Hash, found #{data}"
    end
    
    [:top, :bottom, :left, :right].each do |name|
      @empty = false
      raise "Bad data #{data[name]}" unless data[name].is_a? Numeric 
      instance_variable_set("@#{name}", data[name]);
    end

    raise "Bad horizontal bounds" if @left > @right
    raise "Bad vertical bounds" if @top > @bottom
  end

  def << (bbox)
    if @empty
      @top    = bbox.top
      @bottom = bbox.bottom
      @left   = bbox.left
      @right  = bbox.right
      @empty = false
    else    
      @top    = [@top, bbox.top].min
      @bottom = [@bottom, bbox.bottom].max
      @left   = [@left, bbox.left].min
      @right  = [@right, bbox.right].max
    end
  end

  attr_reader :top, :bottom, :left, :right
end

class Status
  def initialize(root, canvas, macros)
    @root = root
    @canvas = canvas
    @macros = macros
    @current_line = Line.new
    @current_color = 'black'
    @document = Document.new
    @vertical_shift = :normal
    @cursor_position = Point.new(0,0)
    @pen = :up
    @old_macro_bbox = BBox.new
    @global_scale = 1.0

    set_macro_glue(0.5)
    
    load_page(1)
  end

  attr_reader :root

  def set_global_scale(s)
    @global_scale = s
  end

  def set_macro_glue(g)
    @macro_glue=g
  end
   

  def color=(x)
    @current_color=x
  end

  def pen(status, x, y)
    case status
    when :up
      @pen = :up
      close_current_line
      
    when :down
      @pen = :down
      @cursor_position = Point.new(x, y)
      @old_macro_bbox = BBox.new
      @current_line = Line.new(@cursor_position, @current_color)

    else
      raise "Bad pen status #{status}"
    end
  end
  
  def current_page_number 
    @working_page_number
  end
  
  def next_page(create_if_needed)
    assert_leq(current_page_number, @document.last_page_number + 1)

    if is_current_page_new? then
      assert_eq(current_page_number, @document.last_page_number + 1)

      #
      # The current page is "fresh" since its page number is larger
      # than the last page saved in the document.  If next_page
      # was called because the user pressed a right arrow we do nothing
      # (create_if_needed is false), but if the user pressed space
      # we need to create a new page, unless the current page is empty
      # (we did no work on it yet and it makes no sense to create
      # a new page)
      #
      if (! @working_page.empty?) && create_if_needed then
	load_page(current_page_number+1)
	redraw_page
	
	assert_eq(current_page_number-1, @document.last_page_number)
      end
      
    else
      goto_page([current_page_number+1, @document.last_page_number].min)
    end
  end

  def prev_page
      goto_page([current_page_number-1, 1].max)
  end

  def goto_page(page_number)
    assert_geq(page_number, 1)
    assert_leq(page_number, @document.last_page_number)
    
    return if page_number == current_page_number

    load_page(page_number)

    redraw_page
  end
  
  def superscript
    case @vertical_shift
    when :sub
      @vertical_shift = :subsup

    when :normal
      @vertical_shift = :sup
    else
      # supsub does not change
    end
  end

  def subscript
    case @vertical_shift
    when :sup
      @vertical_shift = :supsub

    when :normal
      @vertical_shift = :sub
    else
      # supsub does not change
    end
  end

  def supsub_done
    case @vertical_shift
    when :supsub
      @vertical_shift = :sub

    when :subsup
      @vertical_shift = :sup

    else
      @vertical_shift = :normal
    end
  end

  def current_scale
    if @vertical_shift == :normal
      @global_scale
    else
      0.7 * @global_scale
    end
  end

  def macro_origin
    case @vertical_shift
    when :normal
      @cursor_position

    when :sup, :supsub
      @cursor_position + Point.new(0, @old_macro_bbox.top) * @global_scale

    when :sub, :subsup
      @cursor_position + Point.new(0, @old_macro_bbox.bottom) * @global_scale
      
    end
  end

  def advance(x, y=0)
    @cursor_position += Point.new(x,y)
  end

  def set_position(x,y)
    @cursor_position = Point.new(x,y)
  end
  
  def new_point(x, y, mouse_status)
    return if @pen == :up
    
    @cursor_position = Point.new(x,y)
    @current_line << @cursor_position
    @current_line.draw(@canvas, true)
  end

  def add_macro(macro_name)
    raise "Unknown macro #{macro_name}" unless @macros.is_defined?(macro_name)
    macro = @macros[macro_name]

    close_current_line

    macro.lines.each do
      |line|

      transformed = line * current_scale  + macro_origin;
      transformed.color = @current_color
      @working_page << transformed
    end

    
    delta = (macro.shift + Point.new(@macro_glue, 0.0)) * current_scale 
    
    supsub_done

   
    if @vertical_shift == :normal
      @old_macro_bbox = macro.bbox
      @cursor_position += delta
    end

    redraw_page
  end

  def redraw_page
    @canvas.delete(:all)

    @working_page.lines.each do |line|
      line.draw(@canvas)
    end
  end

  private

  def is_current_page_new?
    current_page_number == @document.last_page_number + 1
  end
  
  def load_page(page_number)
    close_working_page
    
    if page_number < 1 || page_number > @document.last_page_number + 1
      raise "Bad page number #{page_number}"

    elsif page_number == @document.last_page_number + 1
      @working_page = Page.new(page_number)

    else
      @working_page = @document[page_number]
      
    end

    @working_page_number = page_number
  end
  
  def reset_status
    @current_color = "black"
    @cursor_position = Point.new(0,0)
  end

  def close_working_page
    return if @working_page.nil?

    
    @document[@working_page_number] = @working_page
    @working_page = nil
    reset_status
  end
  
  def close_current_line
    return if @current_line.nil?
    
    if @current_line.length > 1
      @working_page << @current_line
    end
    
    @current_line = nil
  end
end

class Document
  def initialize
    @pages = []
  end

  def last_page_number
    @pages.length
  end

  def [](page_number)
    if page_number < 1 || page_number > last_page_number then
      raise "Page out of range: #{page_number}"
    else
      @pages[page_number-1]
    end
  end

  def []=(page_number, page)
    if page_number < 1 || page_number > last_page_number+1 then
      raise "Page out of range: #{page_number}"
      
    elsif page_number == last_page_number + 1 then
      @pages << page
      
    else
      @pages[page_number-1]=page
    end
  end

  def dump(target)
    device = nil
    print_header(target)
    @pages.each { |page| page.dump(target,device) }
    print_trailer(target)
  end

  private

  def print_header(target)
    target.puts("%!PS-Adobe-2.0")
    target.puts
    target.puts("%%Pages: #{pages.length}")
  end

  def print_trailer(target)
    target.puts "%%EOF"
  end
end

class Page
  def initialize(number)
    @lines = []
    @page_number = number
  end

  attr_reader :lines
  attr_reader :page_number

  def empty?
    @lines.empty?
  end

  def <<(line)
    @lines << line
  end

  def undo
    @lines.pop
  end

  def clear
    return if @lines.empty?
    @lines = []
  end

  def dump(target, device)
    print_header(target)
    @lines.each { |line| line.dump(target, device) }
    print_trailer(target)
  end

  private

  def print_header(target)
    target.puts "%%Page: #{@page_number} #{@page_number}"
  end

  def print_trailer(target)
    target.puts "showpage"
  end
end

class Line
  def initialize(line=nil, color='black')
    @coords = case line
	      when NilClass
		[]

	      when Array
		line.dup

	      when Line
		line.coords.dup

	      when Point
		[ line ]

	      else
		raise "Cannot convert a #{line.class} to a Line"
	      end
    
    @color  = color
  end

  attr_accessor :color
  attr_reader   :coords

  def rgbcolor
    case @color
    when Array
      @color

    when "red"
      [1.0, 0.0, 0.0]

    when "green"
      [0.0, 1.0, 0.0]

    when "blue"
      [0.0, 0.0, 1.0]

    when "black"
      [0.0, 0.0, 0.0]

    else
      raise "Bad color: '#{@color}'"
    end
  end

  def empty?
    @coords.empty?
  end

  def length
    @coords.length
  end

  def <<(point)
    @coords << point
  end

  def flattened_coords
    @coords.map {|p| [p.x, p.y]}.flatten    
  end

  def draw(canvas, last_segment=false)
    return if @coords.length < 2

    flattened = self.flattened_coords

    if last_segment
      flattened = flattened[-4..-1]
    end

    color=@color 
    
    TkcLine.new(canvas, flattened) do
      fill(color)
    end
  end

  def clear
    @coords = []
  end

  def dump(target, device)
    target.puts("#{rgbcolor} setcolor")


    action = "moveto"
    @coords.each do |point|
      target.puts("#{device.to_device(point)} #{action}")
      action = "lineto"
    end

    target.puts("stroke")
  end

  def *(scale)
    coords = @coords.map {|x| x * scale}
    Line.new(coords, @color)
  end

  def +(point)
    coords = @coords.map {|x| point + x}
    Line.new(coords, @color)
  end

  def bbox
    return BBox.new if @coords.empty?

    top = bottom = @coords[0].y
    left = right = @coords[0].x

    @coords.each do |p|
      top    = [top, p.y].min
      bottom = [bottom, p.y].max
      left   = [left, p.x].min
      right  = [right, p.x].max
    end

    return BBox.new(:top    => top,
                    :bottom => bottom,
                    :left   => left,
                    :right  => right)
  end
end

class Point
  def initialize(x,y)
    @x=x
    @y=y
  end

  attr_reader :x, :y

  def to_s
    "#{@x} #{@y}"
  end

  def *(s)
    if s.is_a? Numeric then
      Point.new(@x*s, @y*s)
    elsif s.is_a? Array
      Point.new(@x*s[0], @y*s[1])
    else
      raise "Point scale: Numeric or Array expected, found #{s}"
    end
  end

  def +(q)
    Point.new(@x+q.x, @y+q.y)
  end
end

def to_point_array(x)
  if x.length % 2 == 1
    raise "Coordinate array with odd length #{x.length}"
  end

  result = [];
  i=0
  while i < x.length
    result << Point.new(x[i], x[i+1])
    i += 2
  end

  return result
end

def to_line(item)
  if item.is_a? Line
    item
	
  elsif item.is_a? Array
    if item[0].is_a? Point
      Line.new(item)

    elsif item[0].is_a? Numeric
      Line.new(to_point_array(item))

    else
      raise "Cannot convert an array of #{item[0].class} to Line"
    end

  else
    raise "Cannot convert a #{item.class} to a Line"
  end
end

def max_x_of(line)
  line.maps { |point| point.x }.max
end

class Macro
  def initialize(lines, scale=1.0, shift=nil)
    @lines = lines.map { |item| to_line(item) * scale }

    @bbox = BBox.new
    @lines.each { |line| @bbox << line.bbox }

    if shift.nil?
      @shift = Point.new(@bbox.right + 0.5*scale, 0)

    elsif shift.is_a? Numeric
      @shift = Point.new(shift*scale, 0)

    elsif shift.is_a? Point
      @shift = scale * shift

    else
      raise "shift is a #{shift.class}"
    end
  end

  attr_reader :lines
  attr_reader :shift
  attr_reader :bbox
end

def make_line(interp, parameters)
  coords = interp.evaluate(parameters)

  raise "Bad coordinate list" unless coords.all? {|x| x.is_a? Numeric}
  raise "Even length expected" unless coords.length % 2 == 0

  return to_line(coords)
end

def make_macro(interp, parameters)
  #
  # Why the flatten? We can have two kind of calls to macro
  #
  #    (macro (line ...) (line ...) (line ...))
  #
  # or 
  #
  #    (macro (scale x (line ...) (line ...) (line ...)))
  #
  # In the first case lines is an array of lines, while in
  # the second case lines is an array with just one element
  # that is an array of lines.
  #
  # The flatten removes the further Array level
  #

  lines = interp.evaluate(parameters).flatten

  raise "Macro with no lines" if lines.empty?

  lines.each do |item|
    raise "macro expects lines, found #{item.class}" unless item.is_a? Line
  end
    

  return Macro.new(lines)
end

def scale(interp,param)
  data = interp.evaluate(param)

  raise "Bad scale call" unless data.length >= 2

  sx = data.shift

  raise "Translate: Float expected found #{sx}" unless sx.is_a? Numeric

  sy = if data[0].is_a? Numeric then
	data.shift
       else
	 sx
       end

  raise "Bad scale call: no lines found" if data.empty?
  
  result = data.map do |item|
    raise "Scale: Line expected found #{item}" unless item.is_a? Line
    item * [sx, sy]
  end

  return result

end
  

def translate(interp,param)
  data = interp.evaluate(param)

  raise "Bad translate call" unless data.length >= 3

  x = data.shift
  y = data.shift

  raise "Translate: Float expected found #{x}" unless x.is_a? Numeric
  raise "Translate: Float expected found #{y}" unless y.is_a? Numeric

  delta = Point.new(x,y)

  result = data.map do |item|
    raise "Translate: Line expected found #{item}" unless item.is_a? Line
    item + delta
  end

  return result
end

def global_scale(interp, param)
  args = interp.process_parameters(param, :scale)

  $status.set_global_scale(args.scale)

  return nil
end

def macro_bind(interp,param)
  args = interp.process_parameters(param,
				   Pseudo_Lisp::Spec.new(:keyseq, :type=>String),
				   Pseudo_Lisp::Spec.name(:macro_name))

  name = args.macro_name.value

  args.keyseq = "Key-" + args.keyseq unless args.keyseq.include?('-')
  
  callback = proc { $status.add_macro(name) }
  $status.root.bind(args.keyseq, callback)
end

def fliplr(interp, param)
  args = interp.process_parameters(param,
				   Pseudo_Lisp::Spec.new(:item, :type => [Line, Macro]),
				   Pseudo_Lisp::Spec.optional(:pivot, Float::NAN))

  lines = case args.item 
	  when Macro
	    args.item.lines

	  when Line
	    [args.item]

	  else
	    raise "Macro or Line expected, got #{item.inspect}"
	    
	  end

  pivot = if args.pivot.nan?
	    bbox = BBox.new

	    lines.each {|item| bbox << item.bbox}

	    bbox.left + bbox.right
	  else
	    args.pivot
	  end
  
  lines = lines.map do |line|
    tmp = line.coords.map {|point| Point.new(pivot-point.x, point.y)}
    Line.new(tmp)
  end

  case args.item
  when Macro 
    return Macro.new(lines)

  when Line
    return lines[0]
  end
end

def make_circle(interp, param)
  args = interp.process_parameters(param,
				   :radius,
				   Pseudo_Lisp::Spec.optional(:from, 0.0),
				   Pseudo_Lisp::Spec.optional(:to,   360.0),
				   Pseudo_Lisp::Spec.optional(:center_x, nil),
				   Pseudo_Lisp::Spec.optional(:center_y, 0.0))

  x_radius = y_radius = nil
  
  case args.radius
  when Numeric
    x_radius = y_radius = args.radius

  when Pseudo_Lisp::Expression
    raise "Bad radius value" unless args.radius.type == :list

    values = interp.evaluate(args.radius.value)

    raise "Bad list length #{values}" unless values.length == 2

    p values
    
    x_radius = values[0]
    y_radius = values[1]
  else
    raise "Expected Float or list got #{args.radius}"
  end
  
  if args.center_x.nil?
    args.center_x = x_radius
  end

  center = Point.new(args.center_x, args.center_y)

  if args.to < 0
    args.to = 360 + args.to
  end

  coords = []
  angle = args.from
  step = 10
  
  while angle <= args.to
    phi = Math::PI * angle / 180.0;
    
    coords << center + Point.new(x_radius*Math.cos(phi), y_radius*Math.sin(phi))

    angle += step
  end

  return Line.new(coords)
end

macros = Pseudo_Lisp::Interpreter.new
macros.defun('macro',  proc{|interp,param| make_macro(interp,param)})
macros.defun('line',   proc{|interp,param|  make_line(interp,param)})
macros.defun('circle', proc{|interp,param| make_circle(interp,param)})
macros.defun('translate', proc{|interp,param| translate(interp,param)})
macros.defun('scale',     proc{|interp,param| scale(interp,param)})
macros.defun('fliplr',    proc{|interp,param| fliplr(interp,param)})
macros.defun('bind',      proc{|interp,param| macro_bind(interp,param)})

macros.defun('global-scale', proc{|interp,param| global_scale(interp,param)})


root = TkRoot.new do
  title "Prova"
end

canvas = TkCanvas.new(root) do
  background 'white'
  pack('fill'=> 'both', 'expand' => true)
end

#$letter_x = Macro.new([
#			[0, 1, 2, -1],
#			[0, -1, 2, 1]
#		      ], 10, 2.5);

canvas.bind("Motion",
	    proc{|x, y, mouse| $status.new_point(x, y, mouse)  },
	    "%x %y %s")

canvas.bind("ButtonPress",
	    proc{|x, y| $status.pen(:down, x, y) },
	    "%x %y")

canvas.bind("ButtonRelease",
	    proc{|x, y| $status.pen(:up, x, y) },
	    "%x %y")

root.bind("Alt-Control-r", proc{ $status.color="red"   })
root.bind("Alt-Control-b", proc{ $status.color="blue"  })
root.bind("Alt-Control-k", proc{ $status.color="black" })
root.bind("Alt-Control-g", proc{ $status.color="green" })


# root.bind("Key-x", proc{ $status.add_macro($letter_x) })
root.bind("Key-space", proc{ $status.next_page(true) })
root.bind("Right", proc{ $status.next_page(false) })
root.bind("Left", proc{ $status.prev_page })
root.bind("Right", proc{ $status.next_page(false) })
root.bind("Left", proc{ $status.prev_page })
root.bind("Up", proc{ $status.superscript })
root.bind("Down", proc{ $status.subscript })

$status = Status.new(root, canvas, macros)



Macro_Filename = 'macros.rbb'

if File.exists?(Macro_Filename)
  macros.run_file(Macro_Filename)
end


Tk.mainloop

