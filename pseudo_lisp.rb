#!/usr/bin/env ruby

#
# This package defines a kind of LISP-like interpreter, useful to create
# fairly complex configuration files, something more than just a "key, value"
# database, but nothing that requires a fully fledged language
#
# The syntax is the following
#
#    program    = expression+
#    expression = symbol | number | string | '(' program ')'
#
# The native values are numbers, symbols, strings and nil (the empty list)
# however, the callback can produce new types of their own.
#
# The evaluation of a program is as follows:
#
#  - The evaluation of a program is an array of values, each value obtained
#    by evaluating the expressions that make the program
#
#  - The evaluation of a number or a string gives the number/string itself
#
#  - The evaluation of a symbol gives the variable value associated
#    to the symbol
#
#  - The evaluation of a list proceeds as follows
#
#    + The first expression must be a symbol that must have a function
#      definition attached
#
#    + The callback associated to the symbol is called and receives the
#      an array of the remaining expression _unvaluated_ (lazy evaluation)
#
#    + The callback can ask for the evaluation of the parameters or use them
#      "as they are."  This allows to implement also "LISP macros"
#
#    + The callback returns a value (of any Ruby type) that is the result
#      of the evaluation
#

module Pseudo_Lisp
  def Pseudo_Lisp::is_valid_type_spec?(x)
    x.is_a?(Class) || x.is_a?(Symbol)
  end

  Token = Struct.new('Token', 'token', 'value')

  Expression = Struct.new('Expression', 'type', 'value')

  module Basic_Callbacks
    def Basic_Callbacks.unary_op(operation, interp, parameters)
      args = interp.process_parameters(parameters, :x)

      raise "Numeric expected got #{args.x.class}" unless args.x.is_a? Numeric

      case operation
      when :minus
	-args.x

      when :abs
	args.x.abs

      when :not
	  ! args.x

      else
	raise "Unknown unary operation #{operation} (this should never happen)"
      end	
    end

    def Basic_Callbacks.binary_op(operation, interp, parameters)
      args = interp.process_parameters(parameters, :x, :y)

      raise "Numeric expected got #{args.x.class}" unless args.x.is_a? Numeric
      raise "Numeric expected got #{args.y.class}" unless args.y.is_a? Numeric

      case operation
      when :sub
	args.x - args.y

      when :div
	args.x / args.y

      when :mod
	args.x % args.y

      else
	raise "Unknown binary operation #{operation} (this should never happen)"
      end	
    end
    
    def Basic_Callbacks.multi_op(operation, interp, parameters)
      values = interp.evaluate(parameters)
      
      result = nil
      
      case operation
      when :sum
	result = 0
	values.each { |x| result += x }

      when :mult
	result = 1
	values.each { |x| result *= x }

      when :and
	  values.each { |x| return false if !x }
	
	return true
      when :or
	  values.each { |x| return true if x }
	
	return false

      end

      return result
    end

    def Basic_Callbacks.define(interp, params)
      args = interp.process_parameters(params,
				       Pseudo_Lisp::Spec.name(:name),
				       :value)


      interp.defvar(args.name.value, args.value)

      return nil
    end

    def Basic_Callbacks.quote(interp, params)
      args = interp.process_parameters(params, Pseudo_Lisp::Spec.quoted(:val))

      return args.val
    end

    def Basic_Callbacks.defun(interp, params)
      args = interp.process_parameters(params,
                                       Pseudo_Lisp::Spec.name(:name),
                                       Pseudo_Lisp::Spec.new(:params,
                                                             :quoted => true,
                                                             :type   => :list),
                                       Pseudo_Lisp::Spec.tail(:body))

      param_names = args.params.map { |item|
        unless item.is_a?(Expression) && item.type == :symbol
          raise "Expected symbol found #{item}"
        end

        item.value
      }

      interp.defun(args.name.to_s,
                   Lisp_Function.new(param_names, args.body))

    end
  end

  class Spec
    class NoDefault
    end
    
    def initialize(name, options=nil)
      @name = name


      defaults = {
	:default => NoDefault.new,
	:quoted => false,
	:type => nil,
        :tail => false
      }

      options = Hash.new if options.nil?

      defaults.each do |key,value|
	options[key]=value unless options.has_key?(key)
      end

      if Pseudo_Lisp::is_valid_type_spec?(options[:type]) then
	options[:type] = [ options[:type] ]
      end

      raise "Bad type spec" unless options[:type].nil? || options[:type].is_a?(Array)


      
      @default = options[:default]
      @quoted  = options[:quoted]
      @type    = options[:type]
      @tail    = options[:tail]

      if @type.is_a? Array
	@type.each do |item|
	  raise "Bad type spec #{item}" unless Pseudo_Lisp::is_valid_type_spec?(item)
	end
      end
    end

    attr_reader :name, :quoted, :default, :expected, :type

    def optional?
      ! @default.is_a?(NoDefault)
    end

    def is_valid?(value)
      if @type.nil?
	true
      else
	@type.any? {|t| if t.is_a? Class then
			  value.is_a?(t)
			  
			elsif t.is_a? Symbol then
			  value.is_a?(Expression) && value.type == t
			  
			else
			  raise "This shouldn't happen #{t}"
			  
			end}
      end
    end
    
    def Spec.optional(name, default)
      Spec.new(name, :default => default)
    end

    def Spec.quoted(name)
      Spec.new(name, :quoted => true)
    end

    def Spec.typed(name, type)
      Spec.new(name, :type => type)
    end

    def Spec.name(x)
      Spec.new(x, :type => :symbol, :quoted => true)
    end

    def Spec.tail(x)
      Spec.new(x, :tail => true, :quoted => true)
    end
  end

  class Program
    def initialize(expressions=Array.new)
      @instructions = expressions
    end

    def <<(instr)
      @instructions << instr
    end

    def empty?
      @instructions.empty?
    end

    def each
      @instructions.each {|x| yield x}
    end

    def clean
      @instructions = Array.new
    end
  end

  class HashStack
    def initialize
      @stack = Hash.new
    end

    def push
      @stack.unshift(Hash.new)
      nil
    end

    def pop
      raise "Too many pops" if @stack.length == 1
      @stack.shift

      nil
    end

    def []=(key, value)
      @stack[0][key]=value
    end

    def [](key)
      level = look_up(key)

      return nil if level.nil?

      return @stack[level][key]
    end

    def has_key?(key)
      level = look_up(key)

      return ! level.nil?
    end

    def n_levels
      @stack.size
    end
    
    private

    def look_up(key)
      0...@stack.size.each do |idx|
        return idx if @stack[idx].has_key?(key)
      end

      return nil
    end
  end
  
  class Interpreter
    def initialize(define_default_functions = true)
      @functions = Hash.new
      @variables = HashStack.new
      @program   = Program.new

      if define_default_functions
	defun('sum',  proc {|interpreter, params| Basic_Callbacks.multi_op(:sum, interpreter, params)})
	defun('mult', proc {|interpreter, params| Basic_Callbacks.multi_op(:mult, interpreter, params)})
	defun('and',  proc {|interpreter, params| Basic_Callbacks.multi_op(:and, interpreter, params)})
	defun('or',   proc {|interpreter, params| Basic_Callbacks.multi_op(:or, interpreter, params)})

	defun('sub', proc {|interpreter, params| Basic_Callbacks.binary_op(:sub, interpreter, params)})
	defun('div', proc {|interpreter, params| Basic_Callbacks.binary_op(:div, interpreter, params)})
	defun('mod', proc {|interpreter, params| Basic_Callbacks.binary_op(:mod, interpreter, params)})

	defun('minus', proc {|interpreter, params| Basic_Callbacks.unary_op(:minus, interpreter, params)})
	defun('abs', proc {|interpreter, params| Basic_Callbacks.unary_op(:abs, interpreter, params)})
	defun('not', proc {|interpreter, params| Basic_Callbacks.unary_op(:not, interpreter, params)})

	defun('def', proc {|interpreter, params| Basic_Callbacks.define(interpreter, params)})
	defun('quote', proc {|interpreter, params| Basic_Callbacks.quote(interpreter, params)})
      end
    end


    def open_block
      @variables.push
    end

    def close_block
      @variables.pop
    end
    
    def defun(name, callback, override=false)
      raise "Function #{name} existing" if @functions[name] && ! override

      raise "callback should be a Proc, not a #{callback.type}" unless callback.is_a? Proc

      @functions[name]=callback
    end

    def defvar(name, value, override=false)
      raise "Bad name type: #{name.class}" unless (name.is_a?(String) || name.is_a?(Symbol))
      name = name.to_s
      raise "Variable #{name} existing" if @variables[name] && ! override

      @variables[name] = value
    end

    def getvar(name)
      raise "Bad name type: #{name.class}" unless (name.is_a?(String) || name.is_a?(Symbol))
      name = name.to_s
      raise "Variable #{name} undefined" unless @variables.has_key?(name)

      return @variables[name]
    end

    def is_defined?(name)
      raise "Bad name type: #{name.class}" unless (name.is_a?(String) || name.is_a?(Symbol))

      @variables.has_key?(name.to_s)
    end

    def [](name)
      getvar(name)
    end

    def run_file(filename)
      load_file(filename)
      run
    end

    def load_file(filename)
      parse(File.read(filename))
    end

    def parse(program)
      @program = Array.new
      
      tokens = tokenize(program)

      while ! tokens.empty?
	@program << parse_expression(tokens)
      end
    end

    def parse_expression(tokens)
      result = nil

      case tokens[0].token
      when :string, :number
	return Expression.new(:atom, tokens.shift.value)
	
      when :symbol
	return Expression.new(:symbol, tokens.shift.value)
	
      when :open
	tokens.shift
	return parse_list(tokens)

      when :quote
	tokens.shift
	to_be_quoted = parse_expression(tokens)

	return Expression.new(:list, [Expression.new(:symbol, 'quote'), to_be_quoted])
	
      when :close
	raise "Bad expression"
      end
    end

    def parse_list(tokens)
      result = Array.new
      
      loop do
	raise "Missing closed parenthesis" if tokens.empty?

	if tokens[0].token == :closed
	  tokens.shift
	  break
	end

	result << parse_expression(tokens)
      end


      return Expression.new(:list, result)
    end

    def run(die_if_empty=true)
      raise "Run with no program loaded" if @program.empty? && die_if_empty

      @program.map {|expr| evaluate(expr)}
    end

    def evaluate(expression)
      # puts "[#{expression.inspect}]"
      
      case expression
      when Program, Array
	return expression.map {|expr| evaluate(expr)}

      when Expression
	case expression.type
	when :atom
	  return expression.value

	when :symbol
	  name = expression.value.to_s
	  raise "Unknown variable #{name}" unless @variables.has_key?(name)

	  return @variables[name]
	  
	when :list
	  return evaluate_list(expression.value)

	else
	  raise "This should not happpen #{expression.type}"
	end

      else
	raise "This should not happpen #{expression.class}"
      end
    end

    def process_parameters(parameters, *specs)
      params = parameters.dup
      names = Array.new
      values = Array.new
      
      specs.each do |spec|
	spec = Spec.new(spec) if spec.is_a? Symbol

	names << spec.name

	value = if params.empty?
		  raise "Not enough parameters" unless spec.optional?

		  spec.default
		else
		  if spec.quoted
		    params.shift
		  else
		    evaluate(params.shift)
		  end
		end


        unless spec.is_valid?(value)
	  raise "Bad value #{value}, #{value.class}, #{spec.type}"
        end

	values << value
      end

      return Struct.new(*names).new(*values)
    end

    private

    Lisp_Function = Struct.new("Lisp_Function", :names, :body)

    def call_lisp_function(function, params)
      puts "Called function #{function.inspect}"
      raise "Bum"
    end
    
    def evaluate_list(expression)
      expression = expression.dup
      return nil if expression.empty?
      
      function = expression.shift

      raise "This should not happen #{function.class}" unless function.is_a? Expression

      raise "Bad function name #{function}, should be a symbol" unless function.type == :symbol

      function = function.value

      raise "Unknown function #{function}" unless @functions.has_key?(function)
      
      body = @functions[function]

      case body
      when Proc
        return body.call(self, expression)

      when Lisp_Function
        return call_lisp_function(body, expression)

      else
        raise "This should not happen: Proc or Lisp_Function expected, found #{body}"
      end
    end

    def tokenize(program)
      result = Array.new

      program = remove_comments(program).split('')

      while ! program.empty?
	case program[0]
	when /\s/
	  program.shift

	when "'"
	  result << Token.new(:quote, :nil)
	  program.shift
	  
	when '('
	  result << Token.new(:open, :nil)
	  program.shift

	when ')'
	  result << Token.new(:closed, :nil)
	  program.shift

	when '"'
	  result << Token.new(:string, extract_string(program))

	when /[-+0-9]/
	  result << Token.new(:number, extract_number(program))

	else
	  result << Token.new(:symbol, extract_symbol(program))
	end
      end

      return result
    end

    def extract_symbol(program)
      if ! (program[0] =~ /[[:alpha:]]/)
	return program.shift
      end

      value=''

      while !program.empty? && program[0] =~ /[[:word:]-]/
	value << program.shift
      end

      return value
    end

    def extract_string(program)
      raise "String with no \"?!?" unless program[0]=='"'

      program.shift
      value = ''

      loop do
	raise "Missing end \"" if program.empty?

	case program[0]
	when '"'
	  # Here program[0] == '"'.  Eat it
	  program.shift

	  break if program.empty? || program[0] != '"'
	  
	  #
	  # Here we have two consecutive ", we ignore the second one and
	  # add a '"' to the value
	  #
	  value << '"'
	  program.shift

	else
	  value << program.shift
	end
      end

      return value
    end

    def extract_number(program)
      result = ''

      dot_found=false
      exp_found=false
      possible_sign = true
      while ! program.empty?
	case program[0]
	  
	when /[0-9]/
	  result << program.shift
	  possible_sign = false
	  
	when 'e', 'E'
	  raise "Double exp" if exp_found
	  exp_found = true
	  result << program.shift
	  possible_sign = true
	  
	when '.'
	  raise "Double dot" if dot_found
	  dot_found = true
	  result << program.shift
	  possible_sign = false

	when '-', '+'
	  break unless possible_sign
	  result << program.shift
	  possible_sign = false

	else
	  break
	end
      end

      if dot_found || exp_found then
	return result.to_f
      else
	return result.to_i
      end
    end

    def remove_comments(program)
      lines = program.split("\n").map do |line|
	if is_comment_line?(line)
	  ''
	else
	  line
	end
      end

      return lines.join(' ')
    end

    def is_comment_line?(line)
      line =~ /^[[:space:]]*;/
    end
  end
end

# def sum(interp, params)
#   params = interp.evaluate(params)
# 
#   result = 0
#   params.each {|x| result+= x}
# 
#   return result
# end
# 
# def mult(interp, params)
#   params = interp.evaluate(params)
# 
#   result = 1
#   params.each {|x| result = result*x}
# 
#   return result
# end
# 
# def define(interp, params)
#   args = interp.process_parameters(params,
# 				   Pseudo_Lisp::Spec.name(:name),
# 				   :value)
# 
#   interp.defvar(args.name, args.value)
# 
#   return nil
# end

if $0 == __FILE__

  i = Pseudo_Lisp::Interpreter.new

  i.defvar('pippo', 3.14)
  i.parse("(sum 1 2 3 4 5 10) (sum 3 (mult 7 pippo)) (sum pippo pippo) (def zorro 2.7) (def pi 3.14) (sum pi zorro) () '(3 4) 'zorro")
  p i.run

  puts i[:pi]
end
